import cv2
from imutils import rotate

cap = cv2.VideoCapture('./data/teszt01.mp4')

while cap.isOpened():
    ret, frame = cap.read()

    if ret:
        frame = rotate(frame, -90)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2LAB)

        cv2.imshow('frame', gray)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    else:
        break

cap.release()
cv2.destroyAllWindows()
